
var contactBlock = document.querySelector(".contacts-container");
var btn = document.querySelector(".intro__button");

function handleButtonClick() {
  contactBlock.scrollIntoView({ block: "center", behavior: "smooth" });
}

btn.addEventListener("click", handleButtonClick);